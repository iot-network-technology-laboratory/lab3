#include<stdio.h>
#include<conio.h>

int dg=16,dm,dt,data[50],gen[17]={1,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,1};
int si[16],so[16],tx;

void main() {

 void crc(int msg[]);
 int i,j,k;

 //clrscr();

 printf("\n Enter your choice (1/2)\n");
 printf("1 for CRC-CCITT \n");
 printf("2 for generalised generator polynomial\n");
 printf("\n Choice:");

 if(getchar()=='2') {
  printf("\n Enter the degree of generator polynomial\n");
  scanf("%d",&dg);
  printf("\n Enter the generator polynomial\n");
  for(i=dg;i>=0;i--) scanf("%d",&gen[i]);
 }

 printf("\n The generator polynomial is \n");
 for(i=dg;i>=0;i--) printf("%d",gen[i]);

 printf("\n Enter the degree of message\n");
 scanf("%d",&dm);
 printf("\n Enter the message\n");
 for(i=dm;i>=0;i--) scanf("%d",&data[i]);

 dt=dm+dg;
 for(i=0;i<=dm;i++) data[dt-i]=data[dm-i];
 for(i=1;i<=dg;i++) data[dg-i]=0;

 tx=1;
 crc(data);

 printf("\n Enter the received message\n");
 for(i=dt;i>=0;i--) 
 scanf("%d",&data[i]);

 tx=0;
 crc(data);
 getch();
}

void crc(int msg[]) {
int i,j,k,flag;

for(i=0;i<dg;i++) {
  so[i]=0;
  si[i]=0;
     }
for(i=dt;i>=0;i--) {
  if(gen[0]==1) si[0]=so[dg-1]^msg[i];
  else si[0]=msg[i];

  for(j=1;j<=dg-1;j++)
   if(gen[j]==1) si[j]=so[dg-1]^so[j-1];
   else si[j]=so[j-1];

  printf("\n");
  for(k=dg-1;k>=0;k--) {
   so[k]=si[k];
   printf("%d",so[k]);
  }
 }

 if(tx) {
  printf("\n CRC code is \n");
  for(k=dg-1;k>=0;k--) {
   printf("%d",so[k]);
   msg[k]=so[k];
  }

  printf("\n Message to be transmitted\n");
  for(i=dt;i>=0;i--) printf("%d",msg[i]);
 }

 if(!tx) {
  flag=0;
  for(i=0;i<=dg-1;i++)
   if(so[i]==1) {
    flag=1;
    break;
   }
  if(flag==0) printf("\nResult: No error");
  else printf("\n Result: Error in the received msg");
 }
}